<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-home">
        
        <?php include('includes/menu.php') ?>

        <div class="content-carousel">
            <div class="owl-carousel">
                <div class="itemslide" style="background-image: url('assets/images/slide/bgslider.jpg')">
                    <div class="container">
                        <div class="col-lg-8">
                            <div alt="assets/images/slide/thumb/dot-01.png" class="description">
                                <h3 class="title">Quer qualidade<br>nas suas ligações<span class="title-gradient">?</span></h3>
                                <p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, iure eum corporis commodi placeat sit officia dicta illo quae nulla, dolores reprehenderit id eligendi quod est aut voluptas, molestias molestiae?</p>
                                <a class="btn-default" href="#.">Saiba como funciona</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="itemslide" style="background-image: url('assets/images/slide/bgslider.jpg')">
                    <div class="container">
                        <div class="col-lg-8">
                            <div alt="assets/images/slide/thumb/dot-01.png" class="description">
                                <h3 class="title">Quer qualidade<br>nas suas ligações<span class="title-gradient">?</span></h3>
                                <p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, iure eum corporis commodi placeat sit officia dicta illo quae nulla, dolores reprehenderit id eligendi quod est aut voluptas, molestias molestiae?</p>
                                <a class="btn-default" href="#.">Saiba como funciona</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="especialties" class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="especiality  wow fadeInLeft">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 esp1 -aux">
                                    <i class="fas fa-edit icon"></i>
                                    <p> 
                                        <span>Faça cadastro no site</span><br> 
                                        para experimentar nossos serviços sem<br> compromisso ! o cadastro e os<br> testes são gratuitos
                                    </p>
                                </div>

                                <div class="col-lg-6 esp2 -aux">
                                    <i class="fas fa-phone icon"></i>
                                    <p>
                                        <span>Está com dúvidas?</span><br>
                                        Entre em contato conosco estamos<br> esperando ansiosos pelo seu<br> contato
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="functions">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title">Conheça as nossas<br>principais <span class="title-gradient">funções</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">01</span><br>
                            <h3 class="subtitle">Chega de linhas ocupadas</h3>
                            <p>Diferente de uma linha telefônica convencional, nosso serviço oferece no mínimo 4 canais de comunicação.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">02</span><br>
                            <h3 class="subtitle">Elimine as inúmeras<br>linhas sobressalentes</h3>
                            <p>Alguns de nossos clientes possuiam inúmeras linhas de telefône para atender a sua empresa, inúmeros aparelhos de telefone para apenas 1 atendente.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">03</span><br>
                            <h3 class="subtitle">Auto atendimento</h3>
                            <p> A "porta de entrada" da sua empresa nunca esteve tão elegante e profissional, todos os clientes contam com o serviço de mensagem personalizada para o atendimento, de modo rápido e fácil nós fazemos todo o trabalho dificil pra você basta apenas nos informar o texto da mensagem.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">04</span><br>
                            <h3 class="subtitle">Caixa postal por E-Mail</h3>
                            <p>Cada chamada que não puder ser atendida, seja por excesso de tempo na espera ou nenhum atendente disponível, será direcionada a uma caixa postal que enviará a mensagem completa diretamente para o seu e-mail com o audio, o número do telefone, o tempo de duração e o horário da chamada.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">05</span><br>
                            <h3 class="subtitle">Música de espera personalizada</h3>
                            <p>Enquanto seu cliente não é atendido, você pode querer que ele saiba mais sobre sua empresa ou que ele saiba sobre as promoções especiais do dia de hoje ou qualquer tipo de informativo que você deseje.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">06</span><br>
                            <h3 class="subtitle">Distribuição por ramais</h3>
                            <p>Mesmo no pacote mais básico nossos clientes têm direito a no mínimo 4 ramais, portanto se sua empresa esta começando esse é o investimento ideal e na medida para você.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">07</span><br>
                            <h3 class="subtitle">Siga-Me</h3>
                            <p>Em caso de falta de internet ou problemas nos equipamentos, a função siga-me serve para redirecionar as chamadas para qualquer outro número que esteja disponível sendo ramal, telefone fixo ou celular.</p>
                        </div>
                        <div class="col-lg-6 wow fadeInUp">
                            <span class="title-float">08</span><br>
                            <h3 class="subtitle">Sem fidelidade</h3>
                            <p>Todos os nossos serviços são pré pagos e sem limite mínimo de tempo para uso. Em caso de dúvidas veja o depoimento de nossos clientes ou solicite uma demonstração. </p>
                        </div>
                        <div class="col-lg-6 offset-lg-6 wow fadeInUp">
                            <span class="title-float">09</span><br>
                            <h3 class="subtitle">Ramal Universal</h3>
                            <p>Consiste em um ramal de uso público da nossa central, ou seja, livre para todos os clientes e não clientes, totalmente gratuito e de fácil instalação.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="functions-advanced">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title">Veja nossas<br>funções <span class="title-gradient">avançadas</span></h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6 cursor wow fadeInLeft video-btn" data-toggle="modal" data-src="https://www.youtube.com/embed/Jfrjeg26Cwk" data-target="#myModal">
                    <div class="fadv fadv1">
                        <div class="description">
                            <h4>Ligue a vontade</h4>
                            <p>Chamadas ilimitadas tanto para Celulares quanto para Fixos de todo o País, em cada grupo de clientes disponibilizamos 1 Canal para Móvel e 3 para Fixo</p>
                            <a class="float-right" href="#."><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 cursor wow fadeInRight" data-toggle="modal" data-target="#exampleModal">
                    <div class="fadv fadv2">
                        <div class="description">
                            <h4>Gravação de chamadas</h4>
                            <p>Todas as chamadas são gravadas automáticamente e não é necessário protocolo para recuperá-las, apenas acesse nosso site com seu cadastro</p>
                            <a class="float-right" href="#."><i class="flaticon-right-arrow"></i></a> 
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </section>

    <section id="conection">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <h3 class="title">Conectividade</h3>
                    <p>Você pode colocar um ramal do seu PABX em qualquer um desses equipamentos com muita facilidade e comodidade. A tecnologia voip permite que você transforme qualquer dispositivo ligado a internet em uma extensão da sua central telefônica.</p>   
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                        <img src="assets/images/icons/01.png" width="140px">
                        <h3 class="subtitle">Ip phone</h3>
                        <p>Com o telefone ip de mesa, você terá alta qualidade de voz e profissionalismo com todas as funções que nossa central pode oferecer</p>
                    </div>
                </div>
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                    <img src="assets/images/icons/02.png" width="140px">
                        <h3 class="subtitle">Ip wireless</h3>
                        <p>Com o telefone ip sem fio, você pode atender suas chamadas em qualquer lugar da empresa sem ficar preso a sua mesa</p>
                    </div>
                </div>
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                    <img src="assets/images/icons/03.png" width="140px">
                        <h3 class="subtitle">Ata (adaptador)</h3>
                        <p>Vom um adaptador voip, você poderá usar o seu telefone ou pabx atual interligado ao nosssistema sem qualquer mudança na sua estrutura interna</p>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                    <img src="assets/images/icons/04.png" width="140px">
                        <h3 class="subtitle">Ip phone</h3>
                        <p>Com o telefone ip de mesa, você terá alta qualidade de voz e profissionalismo com todas as funções que nossa central pode oferecer</p>
                    </div>
                </div>
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                    <img src="assets/images/icons/05.png" width="140px">
                        <h3 class="subtitle">Ip wireless</h3>
                        <p>Com o telefone ip sem fio, você pode atender suas chamadas em qualquer lugar da empresa sem ficar preso a sua mesa</p>
                    </div>
                </div>
                <div class="col-lg-4 wow fadeInUp">
                    <div class="item-conection">
                    <img src="assets/images/icons/06.png" width="140px">
                        <h3 class="subtitle">Ata (adaptador)</h3>
                        <p>Vom um adaptador voip, você poderá usar o seu telefone ou pabx atual interligado ao nosssistema sem qualquer mudança na sua estrutura interna</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row ocultadesktop">
                <div class="col-lg-6 text-center">
                    <h3 class="title">Serviços</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 wow fadeLeft">
                    <div id="accordion">
                        <?php for($i=0; $i<9; $i++){ ?>
                            <div class="card">
                                <div class="card-header" id="heading<?php echo $i ?>">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>">
                                    Tronco livre
                                    <span class="float-right">R$0,00</span>
                                    </button>
                                </h5>
                                </div>

                                <div id="collapse<?php echo $i ?>" class="collapse" aria-labelledby="heading<?php echo $i ?>" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="bold">Até 10 chamadas por dia<br>
                                                1 Ramal / Extensão<br>
                                                1 Canal de comunicação</p>
                                        </div>
                                        <div class="col-lg-8">
                                            <p>Este serviço contempla todas as funcionalidades do PABX Virtual incluindo WebCallBack, Filas de Espera, etc.</p>
                                            <p>Ideal para quem quer conhecer nossos serviços, incluir em seu website, usar a API para integração ao seu aplicativo ou falar com seus parentes e familiares.</p>
                                            <p>Com 1 Ramal e 1 Canal de comunicação, não é possível realizar chamadas simultâneas porém todas são gravadas e é possível recupera-las a qualquer momento em nosso site.</p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="phrasestruct">
        <div class="container  wow fadeInUp">
            <p>A mais poderosa solução em telefonia</p>
            <h2>Administre seu sistema telefônico<br>empresarial de qualquer lugar.</h2>
            <a href="#." class="btn-transparent mt-3">Entre em contato conosco</a>
        </div>
    </section>

    <section id="crm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title">
                        Conheça nossa<br>integração com <span class="title-gradient">CRM</span>
                    </h2>
                </div>
            </div>
            <div class="row mt-5">
                <?php for($i=0; $i<8; $i++){ ?>
                    <div class="col-lg-6  wow fadeInUp">
                        <div data-toggle="tooltip" data-placement="bottom" title="Ao ligar, seu cliente poderá ser facilmente localizado através de uma dessas opções." class="item-crm">
                            <h3>Consulta via  CPF, CNPJ e telefone</h3>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>                    
    </section> 
    
    <section id="ddd">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title">
                        Veja se existe um DDD<br>disponível em <span class="title-gradient">sua região</span>
                    </h2>
                </div>
            </div>
            <div class="row mt-5 text-center">
                <div class="col-lg-12  wow fadeInUp">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary button-ddd" type="button"><i class="fas fa-search"></i></button>
                        </div>
                        <input type="text" placeholder="Digite aqui sua cidade e veja se existe disponibilidade para você" class="form-control search-ddd" placeholder="" aria-label="" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-lg-12 text-left  wow fadeInUp">
                    <p class="title-table">Localidades disponíveis:</p>
                    <table class="table table-hover tableddd table-responsive">
                        <thead>
                            <tr>
                                <th scope="col">DDD</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Cidade</th>
                                <th scope="col">Operadora</th>
                                <th scope="col">Valor mensal</th>
                                <th scope="col">Contato</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td>Otto</td>
                                <td><button class="btn-default green">Entrar em contato</button></td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td>Otto</td>
                                <td><button class="btn-default green">Entrar em contato</button></td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td>Otto</td>
                                <td><button class="btn-default green">Entrar em contato</button></td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                <td>Otto</td>
                                <td><button class="btn-default green">Entrar em contato</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>                    
    </section>

    <section id="depoimentos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12  wow fadeInUp">
                    <h1 class="title">Depoimentos de nossos clientes</h1>
                    <div class="owl-carousel owl-theme owl-depoimentos">
                        <div class="item">
                            <div class="row">
                                <div class="text-center col-lg-6 offset-lg-3 text-white">
                                <img src="assets/images/depoimentos.jpg" class="img-depoimento">
                                    <p>
                                     Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas eveniet itaque quo rerum quos in officiis, ut eligendi id beatae odit expedita quis sed fuga deserunt soluta tempore modi repellendus?
                                    <br><br><span class="bold">Jhon Snow - SEO WINTER</span>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="text-center col-lg-6 offset-lg-3 text-white">
                                <img src="assets/images/depoimentos.jpg" class="img-depoimento">
                                    <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas eveniet itaque quo rerum quos in officiis, ut eligendi id beatae odit expedita quis sed fuga deserunt soluta tempore modi repellendus?
                                    <br><br><span class="bold">Jhon Snow - SEO WINTER</span>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="promotions">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title">
                        Conheça nossos<br>pacotes <span class="title-gradient">promocionais</span>
                    </h2>
                </div>
            </div>
            <div class="row mt-5 text-center align-items-end  wow fadeInUp">
                <!-- Pricing Table-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div class="bg-white h640 p-5 rounded-lg shadow">
                    <h1 class="h6 font-weight-bold mb-4">Alternativo</h1>
                    <h2 class="h1 font-weight-bold">Gratuito<br><span class="font-weight-normal ml-2 f-19">Personalize seu pacote</span></h2>

                    <div class="custom-separator my-4 mx-auto bg-default"></div>

                    <ul class="list-unstyled my-5 text-small text-center">
                        <li class="mb-3">
                         Lorem ipsum dolor sit amet</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                        <li class="mb-3">
                         At vero eos et accusamus</li>
                        <li class="mb-3 text-muted">
                        
                        <del>Nam libero tempore</del>
                        </li>
                        <li class="mb-3 text-muted">
                        
                        <del>Sed ut perspiciatis</del>
                        </li>
                        <li class="mb-3 text-muted">
                        
                        <del>Sed ut perspiciatis</del>
                        </li>
                    </ul>
                    <a href="#" class="btn-default">Solicitar orçamento</a>
                    </div>
                </div>
                <!-- END -->


                <!-- Pricing Table-->
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div class="bg-white h640 p-5 rounded-lg shadow">
                    <h1 class="h6 font-weight-bold mb-4">Starter<br><span class="font-weight-normal ml-2 f-19">Mais popular</span></h1>
                    <h2 class="h1 font-weight-bold">R$99,00
                        <span class="font-weight-normal ml-2 f-18">/mês</span><br>
                        <span class="font-weight-normal ml-2 f-19">Trimestral R$ 270,00<br>Economize 10%</span>
                    </h2>

                    <div class="custom-separator my-4 mx-auto bg-default"></div>

                    <ul class="list-unstyled my-5 text-small text-center font-weight-normal">
                        <li class="mb-3">
                         Lorem ipsum dolor sit amet</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                        <li class="mb-3">
                         At vero eos et accusamus</li>
                        <li class="mb-3">
                         Nam libero tempore</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                        <li class="mb-3 text-muted">
                        
                        <del>Sed ut perspiciatis</del>
                        </li>
                    </ul>
                    <a href="#" class="btn-default">Solicitar orçamento</a>
                    </div>
                </div>
                <!-- END -->


                <!-- Pricing Table-->
                <div class="col-lg-4">
                    <div class="bg-white h640 p-5 rounded-lg shadow">
                    <h1 class="h6 font-weight-bold mb-4">Starter</h1>
                    <h2 class="h1 font-weight-bold">R$35,00
                        <span class="font-weight-normal ml-2 f-18">/mês</span><br>
                        <span class="font-weight-normal ml-2 f-19">Semestral R$ 189,00<br>Economize 10%</span>
                    </h2>

                    <div class="custom-separator my-4 mx-auto bg-default"></div>

                    <ul class="list-unstyled my-5 text-small text-center font-weight-normal">
                        <li class="mb-3">
                         Lorem ipsum dolor sit amet</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                        <li class="mb-3">
                         At vero eos et accusamus</li>
                        <li class="mb-3">
                         Nam libero tempore</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                        <li class="mb-3">
                         Sed ut perspiciatis</li>
                    </ul>
                    <a href="#" class="btn-default">Solicitar orçamento</a>
                    </div>
                </div>
                <!-- END -->
        </div>                    
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title">
                        Entre em<br>contato <span class="title-gradient">conosco</span>
                    </h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6 mb-5  wow fadeInUp">
                    <form action="" id="sendmessage" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <input name="name" class="form-control" placeholder="Digite seu nome" type="text" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input name="email" class="form-control" placeholder="Digite seu e-mail" type="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input name="phone" class="form-control phone-form" placeholder="Digite seu telefone" type="text" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea name="message" class="form-control" placeholder="Mensagem" id="message" cols="30" rows="10" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn-default w-100 float-right h-100">Enviar mensagem</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6  wow fadeInUp">
                    <h3>Por telefone</h3>
                    <p>Qualquer problema ou dúvida que você tiver entre em contato conosco que teremos prazer de ajudar o mais rápido possível. Dispomos de atendimento em tempo real via telefone das 08:30 até 18 horas e também utilizamos um sistema de atendimento remoto extremamente confiável, caso ache melhor é possível também enviar um email.</p>
                    <h1 class="phone">(0800) 042 - 0162</h1>
                    <h3 class="mt-5">Nós ligamos para você</h3>
                    <div class="input-group mb-3 mt-3">
                        <input type="text" class="form-control phone-form phone-input" placeholder="Digite seu telefone" aria-label="Digite seu telefone" aria-describedby="basic-addon2" required>
                        <div class="input-group-append">
                            <button class="btn-default phone-btn btn-outline-secondary" type="button"><i class="fas fa-phone"></i></button>
                        </div>
                    </div>
                    <p>Coloque seu telefone que completaremos a chamada usando o
                    nosso sistema, ao mesmo tempo você pode conferir a qualidade das
                    chamadas.</p>
                </div>
            </div>
        </div>                    
    </section>                      
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>