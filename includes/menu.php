        <div>
            <div class="menu-wrap">
                <nav class="menu" style="padding-top: 50px;">
                    <div class="icon-list">
                        <a href="#header">
                            <img src="assets/images/logo-original.png" style="max-width: 175px" alt="Sufficit" title="Sufficit"
                        </a>
                        <a href="#header-home" class="removeBody" title="Home"><span>Home</span></a>
                        <a href="#functions" class="removeBody" title="Principais funções"><span>Principais funções</span></a>
                        <a href="#services" class="removeBody" title="Serviços"><span>Serviços</span></a>
                        <a href="#promotions" class="removeBody" title="Pacotes"><span>Pacotes</span></a>
                        <a href="#contact" class="removeBody" title="Contato"><span>Contato</span></a>
                    </div>
                </nav>
                <button class="close-button" id="close-button">Close Menu</button>
            </div>
            <button class="menu-button " id="open-button"></button>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light nav-color">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/logo.png" id="logotroca" style="max-width: 175px" alt="Sufficit" title="Sufficit"></a>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#header-home" title="Home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#functions" title="Principais funções">Principais funções</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#services" title="Serviços">Serviços</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#promotions" title="Pacotes">Pacotes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact" title="Contato">Contato</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link cadastrar" href="#" target="_BLANK" title="Cadastrar"><i class="fas fa-edit"></i> Cadastrar</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link login" href="#" target="_BLANK" title="Entrar"><i class="fas fa-sign-in-alt"></i> Entrar</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </nav>