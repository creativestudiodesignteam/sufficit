<form action="" id="sendmessage" method="post">
    <div class="row">
        <div class="col-lg-12">
            <input name="name" class="form-control" placeholder="Digite seu nome" type="text">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <input name="email" class="form-control" placeholder="Digite seu e-mail" type="email">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <input name="phone" class="form-control" placeholder="Telefone" type="tel">
        </div>
        <div class="col-lg-4">
            <input name="fax" class="form-control" placeholder="Fax" type="tel">
        </div>
        <div class="col-lg-4">
            <input name="mphone" class="form-control" placeholder="Celular" type="tel">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <input name="subject" class="form-control" placeholder="Assunto" type="text">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <textarea name="message" class="form-control" placeholder="Mensagem" id="message" cols="30" rows="10"></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <button class="btn-default float-right">Enviar mensagem</button>
        </div>
    </div>
</form>